﻿using UnityEngine;
using System.Collections;

public class cameraScript : MonoBehaviour {

	public Transform target;

	public Vector2 margin;
	public Vector2 smoothing;

	public BoxCollider2D bounds;

	private Vector3 min;
	private Vector3 max;

	public bool isFollowing { get; set; }

	public void Start ()
	{
		min = bounds.bounds.min;
		max = bounds.bounds.max;
		isFollowing = true;
	}

	public void Update ()
	{
		var x = transform.position.x;
		var y = transform.position.y;

		if (isFollowing)
		{
			if (Mathf.Abs (x - target.position.x) > margin.x)
				x = Mathf.Lerp (x, target.position.x, smoothing.x * Time.deltaTime * .5f); // change last value down to smooth out

			if (Mathf.Abs (y - target.position.y) > margin.y)
				y = Mathf.Lerp (y, target.position.y, smoothing.y * Time.deltaTime * .5f);
		}

		var cameraHalfWidth = GetComponent<Camera>().orthographicSize * ((float)Screen.width / Screen.height);

		x = Mathf.Clamp (x, min.x + cameraHalfWidth, max.x - cameraHalfWidth);
		y = Mathf.Clamp (y, min.y + GetComponent<Camera> ().orthographicSize, max.y - GetComponent<Camera> ().orthographicSize);

		transform.position = new Vector3 (x, y, transform.position.z);
	}

}